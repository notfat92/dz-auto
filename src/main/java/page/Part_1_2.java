package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class Part_1_2 {

  //  @FindBy(xpath = "//input[@placeholder='Поиск']")
    private WebElement searchtest;

    private WebDriver driver;
    public Part_1_2 (WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }
    public Part_1_2 setSearch11(String search2){
        searchtest.sendKeys("Лонгслив White&Green");
        return this;
    }
}