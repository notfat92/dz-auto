package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Part_2_2 {
    @FindBy(id = "input_1496239431201")
    private WebElement searchFIO;

    private WebDriver driver;
    public  Part_2_2 (WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public Part_2_2 FIO (String search22) {
        searchFIO.sendKeys("Зубенко Михаил Петрович");
        return this;
}
