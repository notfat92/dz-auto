import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.Part_1_2;


public class Part_1 {

    private Part_1_2 Page;

    @Test
    public void case1 () {
        //Поиск Лонгслива
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        Page = new Part_1_2(driver);
        SoftAssertions softAssertions = new SoftAssertions();
        driver.get("https://homebrandofficial.ru/wear");


        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@placeholder='Поиск']"))).click();
       driver.findElement(By.xpath("//input[@placeholder='Поиск']")).
               sendKeys("Лонгслив White&Green");
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.
                        xpath("//*[@class='t-store__search-icon js-store-filter-search-btn']"))).click();

        // Ожидание перед проверкой
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.
                        xpath("//*[text()='Лонгслив White&Green']")));

        //Проверка на имя
        By item1 = By.xpath("//*[text()='Лонгслив White&Green']");
        WebElement item = driver.findElement(item1);
        String itemname = "Лонгслив White&Green";
        softAssertions.assertThat(item.getText()).as("Неправильное название").isEqualToIgnoringCase(itemname);

        // Проверка на цену
        By price1 = By.xpath("//*[text()='2 800']");
        WebElement price = driver.findElement(price1);
        String pricename = "2 800";
        softAssertions.assertThat(price.getText()).as("Неверная цена").isEqualToIgnoringCase(pricename);

        //Проверка на количество
        By quantity1 = By.xpath("//*[text()='1']");
        WebElement quantity = driver.findElement(quantity1);
        String quantityname = "1";
        softAssertions.assertThat(quantity.getText()).as("Неверное количество").isEqualToIgnoringCase(quantityname);
        softAssertions.assertAll();
    }
}
