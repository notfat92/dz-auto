import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.Part_2_2;

import java.util.concurrent.TimeUnit;

public class Part_2 {

    private Part_2_2 PageTest;

    @Test
    public void case2 () {System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32\\chromedriver.exe");
       // Оформление заказа
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        PageTest = new Part_2_2(driver);

        SoftAssertions softAssertions = new SoftAssertions();
        driver.get("https://homebrandofficial.ru/wear");
        driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);

        // Нажать на товар с названием “Футболка Оversize”
        new WebDriverWait(driver, 5).until(ExpectedConditions.presenceOfElementLocated(By.
                xpath("//div[@class='js-store-prod-name js-product-name t-store__card__title t-name t-name_md'][text()='Футболка Оversize']"))).click();
        driver.findElement(By.xpath("//*[text()='добавить в корзину']")).click();
        driver.findElement(By.xpath("//*[@class='t706__carticon-img']")).click();
        // Нажать ОФОРМИТЬ ЗАКАЗ
        driver.manage().timeouts().pageLoadTimeout(6, TimeUnit.SECONDS);
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@class='t706__sidebar-continue t-btn']"))).click();

        //Попытка выполнить 2 часть ДЗ
        PageTest.FIO();

        //Ввод данных для оформления заказа
        driver.findElement(By.id("input_1496239431201")).sendKeys("Зубенко Михаил Петрович");
        driver.findElement(By.id("input_1630305196291")).sendKeys("Томск ул. Ленина 10 кв. 1");
        //Ввод номера телефона
        driver.findElement(By.xpath("//input[@style='color: rgb(94, 94, 94);']")).sendKeys("0000000000");
        // Выбор города
        driver.findElement(By.xpath("//input[@class='searchbox-input js-tilda-rule t-input']")).click();
        driver.findElement(By.xpath("//input[@class='searchbox-input js-tilda-rule t-input']")).clear();
        driver.findElement(By.xpath("//input[@class='searchbox-input js-tilda-rule t-input']"))
                .sendKeys("Томск");
        driver.manage().timeouts().pageLoadTimeout(6, TimeUnit.SECONDS);
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By
                .xpath("//div[@data-full-name='Россия, Томская область, г Томск']"))).click();
        // Ввод ФИО
        driver.manage().timeouts().pageLoadTimeout(6, TimeUnit.SECONDS);
        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@placeholder='Иванов Иван Иванович']")));
        driver.findElement(By.xpath("//input[@placeholder='Иванов Иван Иванович']")).sendKeys("Зубенко Михаил Петрович");
        // Ввод адреса
        driver.findElement(By.xpath("//input[@name='tildadelivery-street']")).click();
        driver.findElement(By.xpath("//input[@name='tildadelivery-street']")).clear();
        driver.findElement(By.xpath("//input[@name='tildadelivery-street']")).sendKeys("пл Ленина"+ Keys.TAB);
        // Ввод дома
        driver.findElement(By.xpath("//input[@name='tildadelivery-house']")).click();
        driver.findElement(By.xpath("//input[@name='tildadelivery-house']")).clear();
        driver.findElement(By.xpath("//input[@name='tildadelivery-house']")).sendKeys("д. 10"+ Keys.TAB);
        // Нажать н кнопку ОФОРМИТЬ ЗАКАЗ
        driver.findElement(By.xpath("//button[@class='t-submit'][text()='ОФОРМИТЬ ЗАКАЗ']")).click();
        //button[@class="t-submit"][text()="ОФОРМИТЬ ЗАКАЗ"]

        //Проверка на некорректный номер 1
        By number1 = By.xpath("//div[text()='Укажите, пожалуйста, корректный номер телефона']");
        WebElement numberone = driver.findElement(number1);
        String numbername1 = "Укажите, пожалуйста, корректный номер телефона";
        softAssertions.assertThat(numberone.getText()).as("Неправильная проверка теста №1").isEqualToIgnoringCase(numbername1);

        //Проверка на некорректный номер 2
        By number2 = By.xpath("//p[text()='Укажите, пожалуйста, корректный номер телефона']");
        WebElement numbertwo = driver.findElement(number1);
        String numbername2 = "Укажите, пожалуйста, корректный номер телефона";
        softAssertions.assertThat(numberone.getText()).as("Неправильная проверка теста №2").isEqualToIgnoringCase(numbername2);
        softAssertions.assertAll();







    }
}
